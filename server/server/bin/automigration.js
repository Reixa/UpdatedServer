var app = require('../server');

// this loads the accountDb configuration in ~/server/datasources.json
var dataSource = app.dataSources.postgres;

// this automigrates the model



dataSource.automigrate('Information', function(err) {
 if (err) throw err;
  dataSource.disconnect();
});

dataSource.automigrate('Place', function(err) {
 if (err) throw err;
  dataSource.disconnect();
});

dataSource.automigrate('Event', function(err) {
  if (err) throw err;
  dataSource.disconnect();
});

dataSource.automigrate('Participation', function(err) {
  if (err) throw err;
  dataSource.disconnect();
});


dataSource.automigrate('UserProperty', function(err) {
 if (err) throw err;
  dataSource.disconnect();
});


dataSource.automigrate('Invite', function(err) {
  if (err) throw err;
  dataSource.disconnect();
});

dataSource.automigrate('Contact', function(err) {
  if (err) throw err;
  dataSource.disconnect();
});


dataSource.automigrate('Message', function(err) {
  if (err) throw err;
  dataSource.disconnect();
});


dataSource.automigrate('Group', function(err) {
  if (err) throw err;
 dataSource.disconnect();
});

dataSource.automigrate('InAppNotification', function(err) {
  if (err) throw err;
 dataSource.disconnect();
});


/*
dataSource.isActual(models, function(err, actual) {
  if (!actual) {
    dataSource.autoupdate(models, function(err, result) {
      if (err) throw err;
      dataSource.disconnect();
    });
  }
});

*/
