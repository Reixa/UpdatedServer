'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();
var linker = require("../server/Linker")
var express = require('express');
var http = require('http')
var socketio = require('socket.io');

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);

    // Allow GET Users
    const User = app.models.User;
    User.settings.acls.push(
      {
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "ALLOW",
        "property": "*",
        "accessType": "EXECUTE"
      });

    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }

   //linker.checkNewMessages();

  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});





