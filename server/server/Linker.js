var express = require('express');
var http = require('http')
var socketio = require('socket.io');

module.exports = {

    postElementToDB: function (table, params) {
        return fetch("http://ec2-35-180-69-65.eu-west-3.compute.amazonaws.com/api/" + table, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(params),
        }).then((response) => response.json())
            .then((responseJson) => {
                return responseJson
            });
    },

    getTableCount: function (table) {
        return fetch("http://ec2-35-180-69-65.eu-west-3.compute.amazonaws.com/api/" + table + '/count')
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson.events;
            })
            .catch((error) => {
                console.error(error);
            });
    },

    getTableFromDB: function (table, filter = null) {
        var URL = "http://ec2-35-180-69-65.eu-west-3.compute.amazonaws.com/api/" + table;
        if (filter)
            URL += "?filter=" + filter;
        return fetch(URL)
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch((error) => {
                console.error(error);
            });
    },

    checkNewMessages: function () {
        this.getTableFromDB("InAppNotifications").then(messages => {
            messagesAtStart = messages.length
        });

        setInterval(() => {
            this.getTableFromDB("InAppNotifications").then(messages => {
                
            });
        }, 4000);
    }
};